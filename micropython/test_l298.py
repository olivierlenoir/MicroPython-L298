# Author: Olivier Lenoir - <olivier.len02@gmail.com>
# Created: 2020-06-14 21:05:12
# Project: Test L298 Dual H-bridge, MicroPython
# Description:

from l298 import L298
from utime import sleep

motor = L298(2, 4, 5)
motor.speed(500)
motor.forward()
sleep(2)
motor.speed(200)
motor.forward()
sleep(2)
motor.stop()
motor.reverse()
sleep(2)
motor.stop()
